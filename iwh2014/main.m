//
//  main.m
//  iwh2014
//
//  Created by Noemi Quezada on 10/11/14.
//  Copyright (c) 2014 Cache me if you can. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
